# Laravel con Docker

## Crear la carpeta para alojar los proyectos

Linux y macOS

```
mkdir laravel; cd laravel
```

Windows

```
mkdir laravel & cd laravel
```

> Si ya tenemos un repositorio clonado, abriremos un terminal en esa carpeta.

> En Windows, usar CMD.EXE para lanzar los comandos, no PowerShell.

## Descargar contenedor

1. Clonar el repositorio:

    ```
    git clone https://gitlab.com/edfaviel_jr/test_nativo.git
    ```

    o añadirlo como submódulo, si ya tenemos un proyecto Git:

    ```
    git submodule add https://gitlab.com/edfaviel_jr/test_nativo.git
    ```

    > Para que funcione, tiene que estar instalado [el cliente de línea de comandos de Git](https://git-scm.com/downloads).

2. Copiar el fichero `env-example` a `.env`:

    Linux y macOS

    ```
    cd docker && cp env-example .env
    ```

    Windows

    ```
    cd docker & copy env-example .env
    ```

3. Editar el fichero `.env` de la carpeta `docker`:

    - Modificar la ruta de la aplicación para que apunte a la carpeta `docker` poniendo `APP_CODE_PATH_HOST=../docker`
    - Si disponemos de más de una instalación de docker, modificar la variable `COMPOSE_PROJECT_NAME` y asignarle un nombre único para que los contenedores tengan nombres diferentes.
    - Seleccionar la versión de PHP: `PHP_VERSION=7.4`
    - Modificar el driver de base de datos de phpMyAdmin: `PMA_DB_ENGINE=mariadb`

## Nuevo proyecto de Laravel

(Opcional) Reemplazar `app` por el nombre del proyecto a partir de aquí.

### Generar el proyecto

> :warning: En estos comandos, si se ha renombrado app, cambiar solo la última ocurrencia, después de laravel/laravel.

Linux y macOS

```
docker run -it --rm --name php-cli \
    -v "$PWD:/usr/src/app" thecodingmachine/php:7.4-v3-slim-cli \
    composer create-project --prefer-dist laravel/laravel app
```

Windows

```
docker run -it --rm --name php-cli ^
    -v "%CD%:/usr/src/app" thecodingmachine/php:7.4-v3-slim-cli ^
    composer create-project --prefer-dist laravel/laravel app
```

### (Opcional) Poner el proyecto bajo control de versiones

> :warning: No lanzar este comando si se ha instalado docker como submódulo.

El siguiente comando crea un repositorio nuevo de Git en la carpeta del proyecto. 

```
cd app; git init; git add .; git commit -m "Initial commit"; cd ..
```

### Configurar un nuevo sitio en docker 

1. Ir a `docker/nginx/sites` y duplicar `laravel.conf.example` a `app.conf`.

2. Modificar en el fichero `app.conf` estas dos líneas, cambiado `laravel` por el nombre del proyecto:

    ```
    server_name app.test;
    root /var/www/app/public;
    ```

### Editar fichero de hosts

Añadir a `/etc/hosts` (en Windows `C:\Windows\System32\Drivers\etc\hosts`):

```
127.0.0.1	app.test
```

### (Re)arrancar los contenedores

Los comandos de `docker-compose` se lanzan en la carpeta `docker`.

Arrancar los contenedores necesarios:

```
docker-compose up -d nginx mariadb phpmyadmin workspace
```

Y para reiniciar un contenedor concreto:

```
docker-compose restart nginx
```

### Crear la base de datos

1. Acceder a [phpMyAdmin](http://localhost:8080)

    - Servidor `mariadb` y usuario `root/root`.
    - Crear la base de datos `app` y el usuario `app/app`.

2. Editar el .env de la aplicación

    ```
    DB_CONNECTION=mysql
    DB_HOST=mariadb
    DB_PORT=3306
    DB_DATABASE=app
    DB_USERNAME=app
    DB_PASSWORD=app
    ```
    
#### Parche para Windows

La base de datos no arranca correctamente debido al sistema de ficheros en que corre Docker. Para solucionarlo, editar el fichero `docker/docker-compose.yml` y modificar:

1. En la sección `volumes`, alrededor de la línea 24, añadir un volumen nuevo para alojar los datos de mariadb:

    ```yml
      mariadb_data:
        driver: ${VOLUMES_DRIVER}
    ```

2. En la sección correspondiente a `mariadb`, alrededor de la línea 390, editar la sección `volumes` y reemplazar:

    ```yml
    ### MariaDB ##############################################
        ...
          volumes:
            - ${DATA_PATH_HOST}/mariadb:/var/lib/mysql
        ...
    ```

    Por:

    ```yml
    ### MariaDB ##############################################
        ...
          volumes:
            - mariadb_data:/var/lib/mysql
        ...
    ```

3. Reiniciar los contenedores.

### Acceder al sitio web

Página principal: http://app.test

<a href="https://ibb.co/nC7xkRg"><img src="https://i.ibb.co/jRyn4H6/Captura-de-pantalla-2020-11-15-a-las-11-21-17.png" alt="Captura-de-pantalla-2020-11-15-a-las-11-21-17" border="0"></a>

> Si en Windows da un error de permiso denegado, entrar al workspace (ver siguiente sección) y lanzar el comando: `chown -R docker:docker /var/www`.

## Utilidades

### Lanzar comandos en el proyecto (composer, artisan, npm...)

```
docker-compose exec workspace /bin/bash

cd app
```

Y después el comando que necesitemos. Por ejemplo:

```
php artisan tinker
```

o

```
php artisan make:model Tarea -mcr
```

### Consola de MariaDB

```
docker-compose exec mariadb mysql -u root -proot
```

### Sitio predeterminado

El sitio por defecto de nginx en [localhost](http://localhost) muestra el directorio `laravel/public`. Se puede dejar ahí un fichero `index.html` para que no de un error 404.

### Añadir soporte para fechas en castellano

Editar el fichero `.env` de docker y activar la opción `PHP_FPM_INSTALL_ADDITIONAL_LOCALES=true`. 

En la variable `PHP_FPM_ADDITIONAL_LOCALES` escribir la lista de idiomas adicionales, como por ejemplo `es_ES.UTF-8` para castellano.


