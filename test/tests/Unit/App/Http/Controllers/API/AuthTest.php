<?php

namespace Tests\Unit\App\Http\Controllers\API;

use App\Http\Controllers\API\AuthController;
use PHPUnit\Framework\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCheckIfUserCanRegister()
    {
        $user = [
            'name' => 'test',
            'email' => 'test@email.com',
            'password' => '123456',
            'password_confirmation' => '123456',
        ];

        // $user = json_encode($user);
        $register = new AuthController;
        $response = $register->register($user);

        $this->assertTrue(true);
    }
}
