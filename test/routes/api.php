<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'App\Http\Controllers\API\AuthController@register');
Route::post('login', 'App\Http\Controllers\API\AuthController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('user','App\Http\Controllers\API\AuthController@getAuthenticatedUser');
    Route::post('logout','App\Http\Controllers\API\AuthController@logout');

    // Resource for Policies
    Route::resource('policies', 'App\Http\Controllers\API\Policy\PolicyController', ['except' =>['create', 'edit']]);
    Route::get('custom-query','App\Http\Controllers\API\Policy\PolicyController@getPoliciesByCustomSearch');

});
