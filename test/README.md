# Polizas Test


## Contenido 📋
- [Wiki 📙](https://gitlab.com/edfaviel_jr/test_nativo/-/wikis/home)
- [Requerimientos](#requerimientos-)
- [Descargar el proyecto](#descargar-el-proyecto-)
- [Instalación y preparación del entorno](#instalacion-)
- [Contribuir](#contribuir-al-proyecto-)
___
### Requerimientos 🛠
Necesitas esto en tú ordenador:
- [Git](https://git-scm.com/downloads)
- [Composer](https://getcomposer.org)
- Gestor de servicios
  - [XAMPP](https://www.apachefriends.org/es/index.html)
  - [WAMPSERVER](https://sourceforge.net/projects/wampserver/)
- Editor de código
  - [Visual Studio Code](https://code.visualstudio.com/)
  - [Sublime Text](https://www.sublimetext.com/)
  - Etc
___
### Descargar el proyecto 📥
- Instala Git en tú ordenador.
    - Prueba ingresando el comando **_git_** para ver si la instalación fue correcta.
- Abre una nueva cmd y colocate en la ruta donde querrás tener el proyecto.
- Configura tus [credenciales de git](https://www.atlassian.com/es/git/tutorials/setting-up-a-repository/git-config), con los siguientes comandos:
```bash
git config --global user.email "tucorreo@...."
git config --global user.password "tu contraseña"
```
- Ingresa este comando para clonar el proyecto
```bash
git clone https://gitlab.com/edfaviel_jr/test_nativo.git
``` 
___
### Instalacion ⚙
- Descarga el proyecto.
- Instalar paquetes y dependencias.
```bash
    composer install
```
- #### Configurar el entorno
    - Duplica el archivo ".env.example"
    - Renombra el nuevo archivo por ".env"

    - Ejemplo (vista general del proyecto):

        - ![alt text](https://www.tutorialspoint.com/laravel/images/root_directory.jpg)

    - Levanta los servicios de tú gestor preferido (PHP, MySQL, APACHE).
    - Crea una nueva base de datos.
    - Abre el archivo ".env" y configura tus variables de entornos.
    ```bash
    DB_DATABASE=nombre de la base de datos
    DB_USERNAME=nombre de usuario de MySQL
    DB_PASSWORD=contraseña de usuario de MySQL
    ```
    - En una nueva terminal, ingrese los comandos:
        - Generar una nueva key.
        ```bash
            php artisan key:generate
        ```
        - Migrar las tablas.
        ```bash
            php artisan migrate
        ```
        - Ejecutar _seeders_ para el llenado de las tablas.
        ```bash
            php artisan db:seed
        ```
        - Levantar el servicio.
        ```bash
            php artisan serve
        ```
        - Si todo sale bien, ve a **_localhost:8000_** y podrás ver el proyecto.
___
### Contribuir al proyecto 👨‍💻
Accede a la carpeta del proyecto a traves del una ventana del cmd.
- Ramifica tus cambios
```bash
git checkout -b "tú nombre-dev"
```
- Checa si tienes cambios pendientes a subir.
```bash
git status
```
- Agrega tus cambios al _stage_.
```bash
git add -A
```
- Al ejecutar el comando _git add -A_, se añadirán al índice los ficheros borrados, los modificados y los agregados.
- Agregar ese indice como nueva versión en Git.
```bash
git commit -m "descripción de los cambios"
```
- Descarga los ultimos cambios del proyecto
```bash
git pull origin develop
```
- Sube tus cambios a tú rama
```bash
git push origin "tú nombre-dev"
```
___


<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.
