<?php

namespace Database\Factories;

use App\Models\InsurancePolicy;
use Illuminate\Database\Eloquent\Factories\Factory;

class InsurancePolicyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InsurancePolicy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'number_policy' => $this->faker->unique()->word,
            'start' => $this->faker->dateTime(),
            'final' => $this->faker->dateTime(),
            'price' => '300',
            'status' => mt_rand(1,2),
            'user_id' => '1',
            'client_id' => '1',
            'insurance_carrier_id' => mt_rand(1,11),
            'type_id' => mt_rand(1,11),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
