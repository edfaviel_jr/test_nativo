<?php

namespace Database\Factories;

use App\Models\PoliciesInsureds;
use Illuminate\Database\Eloquent\Factories\Factory;

class PoliciesInsuredsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PoliciesInsureds::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'insured_id' => 1,
            'policy_id' => mt_rand(1,11),
        ];
    }
}
