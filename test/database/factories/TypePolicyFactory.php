<?php

namespace Database\Factories;

use App\Models\TypePolicy;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypePolicyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TypePolicy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->word,
        ];
    }
}
