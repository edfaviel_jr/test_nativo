<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeysToInsureds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insureds', function (Blueprint $table) {
            // Foreign Key Constraints
            // $table->unsignedBigInteger('insurance_policy_id');
            // $table->foreign('insurance_policy_id')->references('id')->on('insurance_policies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insureds', function (Blueprint $table) {
            // Foreign Key Constraints
            // $table->dropForeign('policy_id')->references('id')->on('insurance_policies');
            // $table->dropColumn('policy_id');
        });
    }
}
