<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoliciesInsuredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies_insureds', function (Blueprint $table) {
            $table->unsignedBigInteger('policy_id');
            $table->foreign('policy_id')->references('id')->on('insurance_policies')->onDelete('cascade');

            $table->unsignedBigInteger('insured_id');
            $table->foreign('insured_id')->references('id')->on('insureds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies_insureds');
    }
}
