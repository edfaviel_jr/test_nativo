<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeysToInsurancePolicies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_policies', function (Blueprint $table) {
            // Foreign Key Constraints
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('insurance_carrier_id');
            $table->foreign('insurance_carrier_id')->references('id')->on('insurance_carriers');

            $table->unsignedBigInteger('client_id')->nullable();
            $table->foreign('client_id')->references('id')->on('clients');

            // Foreign Key Constraints
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('type_policies');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_policies', function (Blueprint $table) {
            // drop Foreign Key Constraints
            $table->dropColumn('user_id');
            $table->dropForeign('user_id')->references('id')->on('users');

            $table->dropForeign('insurance_carrier_id')->references('id')->on('insurance_carriers');
            $table->dropColumn('insurance_carrier_id');

            $table->dropForeign('client_id')->references('id')->on('clients');
            $table->dropColumn('client_id');

            $table->dropForeign('type_id')->references('id')->on('type_policies');
            $table->dropColumn('type_id');
        });
    }
}
