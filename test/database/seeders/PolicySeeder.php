<?php

namespace Database\Seeders;

use App\Models\InsurancePolicy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('insurance_policies')->insert(
            [
                'number_policy' => '00001',
                'start' => '2019-05-01 03:02:12',
                'final' => '2019-06-01 03:02:12',
                'price' => '300',
                'status' => '2',
                'user_id' => '1',
                'client_id' => '1',
                'insurance_carrier_id' => 1,
                'type_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'number_policy' => '00002',
                'start' => '2019-07-01 03:02:12',
                'final' => '2019-08-01 03:02:12',
                'price' => '600',
                'status' => '2',
                'user_id' => '1',
                'client_id' => '1',
                'insurance_carrier_id' => 1,
                'type_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );
    }
}
