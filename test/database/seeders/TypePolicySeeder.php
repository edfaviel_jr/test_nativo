<?php

namespace Database\Seeders;

use App\Models\TypePolicy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypePolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_policies')->insert([
            'name' => 'gastos médicos',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        TypePolicy::factory()
                            ->count(10)
                            ->create();
    }
}
