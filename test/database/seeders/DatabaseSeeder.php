<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(InsuranceCarrierSeeder::class);
        $this->call(TypePolicySeeder::class);
        $this->call(InsuredSeeder::class);
        $this->call(PolicySeeder::class);
        $this->call(PolicySeeder::class);

    }
}
