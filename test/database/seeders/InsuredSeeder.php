<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InsuredSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('insureds')->insert(
            [
                'name' => 'insured test 1',
                'age' => '26',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'insured test 2',
                'age' => '25',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );
    }
}
