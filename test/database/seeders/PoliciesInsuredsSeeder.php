<?php

namespace Database\Seeders;

use App\Models\PoliciesInsureds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PoliciesInsuredsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('policies_insureds')->insert([
            'policy_id' => 1,
            'insured_id' => 1
        ]);
    }
}
