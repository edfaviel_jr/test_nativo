<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'name' => 'client test lol',
            'email' => 'test_client@example.com',
            'phone' => '123456789',
            'created_at' => now(),
            'updated_at' => now(),
            'user_id' => 1,
        ]);
    }
}
