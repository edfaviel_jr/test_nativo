<?php

namespace Database\Seeders;

use App\Models\InsuranceCarrier;
use Illuminate\Database\Seeder;

class InsuranceCarrierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InsuranceCarrier::factory()
                            ->count(10)
                            ->create();
    }
}
