<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsuranceCarrier extends Model
{
    use HasFactory,SoftDeletes;


     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Set the type policy name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

   /**
    * Get all of the policies for the InsuranceCarrier
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function policies()
   {
       return $this->hasMany(InsurancePolicy::class);
   }
}
