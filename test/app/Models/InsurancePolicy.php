<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsurancePolicy extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number_policy',
        'start',
        'final',
        'price',
        'status'
    ];

    /**
     * The insureds that belong to the InsurancePolicy
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function insureds()
    {
        return $this->belongsToMany(Insured::class, 'policies_insureds');
    }

    /**
     * Get the client that owns the InsurancePolicy
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get the carrier that owns the InsurancePolicy
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carrier()
    {
        return $this->belongsTo(InsuranceCarrier::class);
    }

    /**
     * Get type for the InsurancePolicy
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function type()
    {
        return $this->belongsTo(TypePolicy::class);
    }
}
