<?php

namespace App\Http\Controllers\API\Policy;

use App\Http\Controllers\Controller;
use App\Models\InsurancePolicy;
use Illuminate\Http\Request;



class PolicyController extends Controller
{
    /**
    * @OA\Get(
    *     path="/api/policies",
    *     operationId="index",
    *     tags={"Poliza de seguro"},
    *     summary="Mostrar todas las polizas",
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todas las polizas."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function index()
    {
        $policies = InsurancePolicy::all();
        return response()->json(['data' => $policies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
    * @OA\POST(
    *     path="/api/policies",
    *     summary="Crear Poliza",
    *     operationId="store",
    *     tags={"Poliza de seguro"},
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\RequestBody(
    *     required=true,
    *     description="Crear Poliza",
    *     @OA\JsonContent(
    *           required={"number_policy","start","start","final","price","status","user","carrier","client"},
    *           @OA\Property(property="number_policy", type="string", format="text", example="000001"),
    *           @OA\Property(property="start", type="datetime", format="datetime", example="2021-07-01 04:38:42"),
    *           @OA\Property(property="final", type="datetime", format="datetime", example="2021-08-01 04:38:42"),
    *           @OA\Property(property="price", type="number", format="number", example="300"),
    *           @OA\Property(property="user", type="number", format="number", example="1"),
    *           @OA\Property(property="carrier", type="number", format="number", example="1"),
    *           @OA\Property(property="client", type="number", format="number", example="1"),
    *          ),
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Poliza creada con exito"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function store(Request $request)
    {
        $policy = new InsurancePolicy();
        $policy->number_policy = '1';
        $policy->start = $request->start;
        $policy->final = $request->final;
        $policy->price = $request->price;
        $policy->status = 1;
        $policy->user_id = $request->user;
        $policy->insurance_carrier_id = $request->carrier;
        $policy->client_id = $request->client;
        $policy->saveOrFail();

        return response()->json(['data' => $policy],200);
    }

    /**
    * @OA\GET(
    *     path="/api/policies/{policy_id}",
    *     operationId="show",
    *     tags={"Poliza de seguro"},
    *     summary="Muestra una poliza en concreto",
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\Response(
    *         response=200,
    *         description="Muestra una poliza en concreto."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function show(InsurancePolicy $policy)
    {
        return response()->json(['data' => $policy]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
    * @OA\PUT(
    *     path="/api/policies/{policy_id}",
    *     summary="Actualizar Poliza",
    *     operationId="update",
    *     tags={"Poliza de seguro"},
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\RequestBody(
    *     required=true,
    *     description="Actualizar Poliza",
    *     @OA\JsonContent(
    *           required={"number_policy","start","start","final","price","status","user","carrier","client"},
    *           @OA\Property(property="number_policy", type="string", format="text", example="000001"),
    *           @OA\Property(property="start", type="string", format="datetime", example="2021-07-01 04:38:42"),
    *           @OA\Property(property="final", type="string", format="datetime", example="2021-08-01 04:38:42"),
    *           @OA\Property(property="price", type="number", format="number", example="300"),
    *           @OA\Property(property="user", type="number", format="number", example="1"),
    *           @OA\Property(property="carrier", type="number", format="number", example="1"),
    *           @OA\Property(property="client", type="number", format="number", example="1"),
    *          ),
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Poliza actualizada con exito"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function update(Request $request, InsurancePolicy $policy)
    {
        $policy->number_policy = $request->number;
        $policy->start = $request->start;
        $policy->final = $request->final;
        $policy->price = $request->price;
        $policy->status = $request->status;
        $policy->user_id = $request->user;
        $policy->insurance_carrier_id = $request->carrier;
        $policy->client_id = $request->client;
        $policy->saveOrFail();

        return response()->json(['data' => $policy],200);
    }

    /**
    * @OA\DELETE(
    *     path="/api/policies/{policy_id}",
    *     operationId="delete",
    *     tags={"Poliza de seguro"},
    *     summary="elimina una poliza en concreto",
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\Response(
    *         response=200,
    *         description="elimina una poliza en concreto."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function destroy(InsurancePolicy $policy)
    {
        $policy->delete();

        return response()->json(['data' => 'success'],200);
    }

    /**
    * @OA\GET(
    *     path="/api/custom-query",
    *     operationId="customQuery",
    *     tags={"Poliza de seguro"},
    *     security={{"bearer":{}}},
    *     summary="Obtiene la lista de pólizas del tipo GASTOS MÉDICOS,con estado VENCIDA, con fecha de vigencia menores al 15/Febrero/2021",
    *     @OA\Response(
    *         response=200,
    *         description="Obtiene la lista de pólizas del tipo GASTOS MÉDICOS,con estado VENCIDA, con fecha de vigencia menores al 15/Febrero/2021"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    * )
    */

    public function getPoliciesByCustomSearch()
    {
        $policies = InsurancePolicy::leftJoin('type_policies', function($q)
        {
            $q->on('type_policies.id', '=', 'insurance_policies.type_id')
                ->where('type_policies.name', 'like', "%gastos medicos%");
        })
        ->where('insurance_policies.status','=','2')
        ->where('final','<','2021-02-15')
        ->orderBy('insurance_policies.created_at','DESC')
        ->get(['insurance_policies.*','type_policies.name as type']);

        return response()->json(['data' => $policies], 200);
    }
}
