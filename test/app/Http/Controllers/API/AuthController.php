<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;




class AuthController extends Controller
{
    /**
    * @OA\POST(
    *     path="/api/login",
    *     summary="Autentica al usuario",
    *     operationId="login",
    *     tags={"Auth"},
    *     @OA\RequestBody(
    *     required=true,
    *     description="Credenciales",
    *     @OA\JsonContent(
    *           required={"email","password"},
    *           @OA\Property(property="email", type="string", format="email", example="test@example.com"),
    *           @OA\Property(property="password", type="string", format="password", example="123456"),
    *          ),
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Hizo la autenticacion"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    /**
    * @OA\POST(
    *     path="/api/user",
    *     summary="Comprueba el middleware del JWT",
    *     operationId="user",
    *     tags={"Auth"},
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\Response(
    *         response=200,
    *         description="Authorization Token not found"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        }
        catch (TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
        }
        catch (TokenInvalidException $e) {
                return response()->json(['token_invalid'], $e->getStatusCode());
        }
        catch (JWTException $e) {
                return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }

    /**
    * @OA\POST(
    *     path="/api/register",
    *     summary="Register",
    *     operationId="register",
    *     tags={"Auth"},
    *     @OA\RequestBody(
    *     required=true,
    *     description="Credenciales",
    *     @OA\JsonContent(
    *           required={"name","email","password","password_confirmation"},
    *           @OA\Property(property="name", type="string", format="text", example="test"),
    *           @OA\Property(property="email", type="string", format="email", example="test@email.com"),
    *           @OA\Property(property="password", type="string", format="password", example="123456"),
    *           @OA\Property(property="password_confirmation", type="string", format="password", example="123456"),
    *          ),
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="registra al usuario"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    /**
    * @OA\POST(
    *     path="/api/logout",
    *     summary="Logout",
    *     operationId="logout",
    *     tags={"Auth"},
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         required=true,
    *         description="Bearer {access-token}",
    *         @OA\Schema(
    *              type="string"
    *         )
    *      ),
    *     @OA\Response(
    *         response=200,
    *         description="cierra sesion y elimina el Token"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function logout( Request $request ) {

        $token = $request->header( 'Authorization' );

        try {
            JWTAuth::parseToken()->invalidate( $token );

            return response()->json( [
                'error'   => false,
                'message' => trans( 'bye:)' )
            ] );

        } catch ( TokenExpiredException $exception ) {
            return response()->json( [
                'error'   => true,
                'message' => trans( 'auth.token.expired' )
            ], 401 );
        } catch ( TokenInvalidException $exception ) {
            return response()->json( [
                'error'   => true,
                'message' => trans( 'auth.token.invalid' )
            ], 401 );

        } catch ( JWTException $exception ) {
            return response()->json( [
                'error'   => true,
                'message' => trans( 'auth.token.missing' )
            ], 500 );
        }
    }
}
