<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
* @OA\Info(title="API POLIZAS", version="1.0")
*
* @OA\Server(url="http://localhost:8000")
* @OAS\SecurityScheme(
*      securityScheme="bearer",
*      in="header",
*      bearerFormat="JWT",
*      type="http",
*      scheme="bearer"
* )
*/
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
