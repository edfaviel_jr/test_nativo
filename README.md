# Polizas Test


## Contenido 📋
- [Wiki 📙](https://gitlab.com/edfaviel_jr/test_nativo/-/wikis/home)
- [Requerimientos](#requerimientos-)
- [Descargar el proyecto](#descargar-el-proyecto-)
- [Estructura del proyecto](#instalacion-)
- [Contribuir](#contribuir-al-proyecto-)
___
### Requerimientos 🛠
Necesitas esto en tú ordenador:
- [Git](https://git-scm.com/downloads)
- [Composer](https://getcomposer.org)
- Gestor de servicios
  - [XAMPP](https://www.apachefriends.org/es/index.html)
  - [WAMPSERVER](https://sourceforge.net/projects/wampserver/)
- Editor de código
  - [Visual Studio Code](https://code.visualstudio.com/)
  - [Sublime Text](https://www.sublimetext.com/)
  - Etc
___
### Descargar el proyecto 📥
- Instala Git en tú ordenador.
    - Prueba ingresando el comando **_git_** para ver si la instalación fue correcta.
- Abre una nueva cmd y colocate en la ruta donde querrás tener el proyecto.
- Configura tus [credenciales de git](https://www.atlassian.com/es/git/tutorials/setting-up-a-repository/git-config), con los siguientes comandos:
```bash
git config --global user.email "tucorreo@...."
git config --global user.password "tu contraseña"
```
- Ingresa este comando para clonar el proyecto
```bash
git clone https://gitlab.com/edfaviel_jr/test_nativo.git
``` 
___

### Estructura del proyecto

- docker
    - Contiene los archivos necesarios para correr un contenedor docker de laravel en tu ordenador
- test
    - Contiene el codigo fuente del programa si asi decides no usar docker

___
### Contribuir al proyecto 👨‍💻
Accede a la carpeta del proyecto a traves del una ventana del cmd.
- Ramifica tus cambios
```bash
git checkout -b "tú nombre-dev"
```
- Checa si tienes cambios pendientes a subir.
```bash
git status
```
- Agrega tus cambios al _stage_.
```bash
git add -A
```
- Al ejecutar el comando _git add -A_, se añadirán al índice los ficheros borrados, los modificados y los agregados.
- Agregar ese indice como nueva versión en Git.
```bash
git commit -m "descripción de los cambios"
```
- Descarga los ultimos cambios del proyecto
```bash
git pull origin develop
```
- Sube tus cambios a tú rama
```bash
git push origin "tú nombre-dev"
```
___